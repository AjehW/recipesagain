from django.apps import AppConfig


class BigRecipesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "big_recipes"
